<br><div align="center"><img src="./public/logo_exmobo.png" width="40%"><br><br>

Simplest way to get Node.js app with [Express](https://expressjs.com) and [MongoDB](https://www.mongodb.com).

</div>

## Install

To run it on your own.

```zsh
yarn 		// to install dependencies from 'package.json'
yarn dev 	// to run 'nodemon'
```

## Credentials

You should put your own credentials for connecting MongoDB in `server.js` file. It doesn't matter if its [MLab](https://mlab.com) or `MongoDB` on your device. 
