const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const MongoClient = require('mongodb').MongoClient
const password = process.env.WITCHER_PASSWORD

var db

// change 'robertgrzonka' and '${password} into your credentials
MongoClient.connect(`mongodb://robertgrzonka:${password}@ds161520.mlab.com:61520/node-mongo-express`, {
	useNewUrlParser: true
}, (err, client) => {
	if (err) return console.log(err)
	db = client.db('node-mongo-express')
	app.listen(3000, () => {
		console.log('Nasłuchuję na http://localhost:3000')
	})
})

app.use(bodyParser.urlencoded({
	extended: true
}))
app.use(express.static('public'))

app.get('/', (req, res) => {
	// res.sendFile(__dirname + '/index.html')
	db.collection('quotes').find().toArray(function (err, results) {
		if (err) return console.log(err)
		// renders index.ejs
		res.render('index', {
			quotes: results
		})
	})
})

app.post('/quotes', (req, res) => {
	db.collection('quotes').save(req.body, (err, result) => {
		if (err) return console.log(err)
		console.log('Zapisano!')
		res.redirect('/')
	})
})

app.set('view engine', 'ejs')
